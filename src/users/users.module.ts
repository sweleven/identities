import { HttpModule, Module } from '@nestjs/common';
import { UsersService } from './services/users.service';
import { UsersController } from './controllers/users.controller';
import { KeycloakModule } from 'src/keycloak/keycloak.module';

@Module({
  imports: [HttpModule, KeycloakModule],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
