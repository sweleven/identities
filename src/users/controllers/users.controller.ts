import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  Logger,
} from '@nestjs/common';
import { ApiBody, ApiParam, ApiQuery, ApiResponse } from '@nestjs/swagger';
import { PaginationQueryDto } from 'src/dto/paginationQuery.dto';
import { FindOneParams } from 'src/params/find-one.params';
import { Observable } from 'rxjs';
import { CreateUserDto } from '../dto/create-user.dto';
import { UpdateUserDto } from '../dto/update-user.dto';
import { UsersService } from '../services/users.service';
import { User } from '../entities/user.entity';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  @ApiBody({
    required: true,
    type: CreateUserDto,
  })
  @ApiResponse({
    status: 200,
    description: 'The created record',
    type: User,
  })
  create(@Body() createUserDto: CreateUserDto): Observable<User> {
    return this.usersService.create(createUserDto);
  }

  @Get()
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'offset',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'role',
    required: false,
    enum: ['admin', 'employee', 'cleaner'],
    type: String,
  })
  @ApiResponse({
    status: 200,
    description: 'All existing records',
    type: [User],
  })
  @ApiResponse({
    status: 400,
    description: 'Invalid id format provided',
  })
  @ApiResponse({
    status: 404,
    description: 'No record found',
  })
  findAll(@Query() paginationQuery: PaginationQueryDto): Observable<User[]> {
    Logger.debug(paginationQuery);
    return this.usersService.findAll(paginationQuery);
  }

  @Get(':id')
  @ApiParam({
    name: 'id',
    required: true,
    type: String,
  })
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: User,
  })
  @ApiResponse({
    status: 404,
    description: 'No records found',
  })
  findOne(@Param() { id }: FindOneParams): Observable<User> {
    return this.usersService.findOne(id);
  }

  @Patch(':id')
  @ApiParam({
    name: 'id',
    required: true,
    type: String,
  })
  @ApiBody({
    required: true,
    type: UpdateUserDto,
  })
  @ApiResponse({
    status: 200,
    description: 'The updated record',
    type: User,
  })
  @ApiResponse({
    status: 404,
    description: 'No records found',
  })
  update(
    @Param() { id }: FindOneParams,
    @Body() updateUserDto: UpdateUserDto,
  ): Observable<User> {
    return this.usersService.update(id, updateUserDto);
  }

  @Delete(':id')
  @ApiParam({
    name: 'id',
    required: true,
    type: String,
  })
  @ApiResponse({
    status: 200,
    description: 'The deleted room',
    type: User,
  })
  @ApiResponse({
    status: 404,
    description: 'No records found',
  })
  remove(@Param() { id }: FindOneParams): Observable<void> {
    return this.usersService.remove(id);
  }
}
