import { HttpService, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { combineLatest, Observable, of } from 'rxjs';
import { catchError, filter, map, switchMap } from 'rxjs/operators';
import { PaginationQueryDto } from 'src/dto/paginationQuery.dto';
import { KeycloakInterceptor } from 'src/keycloak/http.interceptor';
import { CreateUserDto } from '../dto/create-user.dto';
import { UpdateUserDto } from '../dto/update-user.dto';
import { User } from '../entities/user.entity';
import { accountActions } from '../enums/accountActions.enum';

export const roles = ['employee', 'cleaner', 'admin'];

@Injectable()
export class UsersService {
  private readonly keycloakAdmin: string;

  constructor(
    private readonly http: HttpService,
    private readonly configService: ConfigService,
    private readonly keycloakInterceptor: KeycloakInterceptor,
  ) {
    // Set interceptor to make authenticated calls
    this.http.axiosRef.interceptors.request.use((request) => {
      return this.keycloakInterceptor.authenticateRequest(request).toPromise();
    });

    // Get keycloak admin base path for all requests
    this.keycloakAdmin = this.configService.get('KEYCLOAK_ADMIN_PATH');
  }

  /**
   * Adds a user
   *
   * @param {CreateUserDto} createUserDto
   * @return {*}  {Observable<User>}
   * @memberof UsersService
   */
  create(createUserDto: CreateUserDto): Observable<User> {
    Logger.debug(`Creating a new User ${createUserDto}`);

    const user = {
      attributes: {
        locale: ['it'],
      },
      email: createUserDto.email,
      emailVerified: true,
      enabled: true,
    };

    return this.http.post(`${this.keycloakAdmin}/users`, user).pipe(
      // Get users
      switchMap(() => {
        return this.searchUser(createUserDto.email);
      }),

      // email is the username and must be unique
      map((users) => {
        if (users) {
          return users[0];
        } else {
          return null;
        }
      }),

      filter((_user) => _user != null),

      // Set user's role
      switchMap((_user: User) => {
        return this.setUserRole(_user.id, createUserDto.role).pipe(
          map(() => _user),
        );
      }),

      // Send email to complete registration
      switchMap((_user: User) => {
        return this.updateAccount(_user.id, [
          accountActions.UPDATE_PASSWORD,
          accountActions.UPDATE_PROFILE,
        ]).pipe(
          // Return user for coherence
          map(() => _user),
        );
      }),

      switchMap((_user) => {
        return this.searchUser(_user.email);
      }),

      catchError((err, caught) => {
        // TODO: Manage returned status code to eventually infere error type
        Logger.log(err);
        return of(null);
      }),
    );
  }

  /**
   * Returns all users
   *
   * @return {*}  {Observable<User[]>}
   * @memberof UsersService
   */
  findAll(paginationQuery: PaginationQueryDto): Observable<User[]> {
    Logger.debug(`Finding all users`);

    const searchQuery = paginationQuery.searchQuery;
    const role = paginationQuery.role;

    let endpoint = `${this.keycloakAdmin}/users`;

    if (role) {
      return this.searchByRole(role);
    }

    if (searchQuery) {
      endpoint = `${endpoint}?search=${searchQuery}`;
    }

    return this.http.get(endpoint).pipe(map((res) => res.data));
  }

  /**
   * Returns the user with the provided `id`
   *
   * @param {string} id
   * @return {*}  {Observable<User>}
   * @memberof UsersService
   */
  findOne(id: string): Observable<User> {
    Logger.debug(`Finding user by id: ${id}`);

    const userInfo = this.getUserFromId(id);

    const userRoles = this.getUserRoles(id);

    return combineLatest([userInfo, userRoles]).pipe(
      map(([_userInfo, _userRole]) => {
        _userInfo['roles'] = _userRole;
        return _userInfo;
      }),
    );
  }

  /**
   * Update user with id `id` (only if it already exists). Returns the
   * modified user
   *
   * @param {string} id
   * @param {UpdateUserDto} updateUserDto
   * @return {*}  {Observable<User>}
   * @memberof UsersService
   */
  update(id: string, updateUserDto: UpdateUserDto): Observable<User> {
    Logger.debug(`Updating user ${id} with ${updateUserDto}`);

    let chain$ = of(null);

    if (
      updateUserDto.email ||
      updateUserDto.firstName ||
      updateUserDto.lastName
    ) {
      chain$ = chain$.pipe(
        switchMap(() => {
          return this.getUserFromId(id);
        }),
        switchMap((_user: User) => {
          _user.email = updateUserDto.email ? updateUserDto.email : _user.email;
          _user.firstName = updateUserDto.firstName
            ? updateUserDto.firstName
            : _user.firstName;
          _user.lastName = updateUserDto.lastName
            ? updateUserDto.lastName
            : _user.lastName;

          return of(_user);
        }),
        switchMap((_user) => {
          return this.http.put(`${this.keycloakAdmin}/users/${id}`, _user);
        }),
      );
    }

    if (updateUserDto.role) {
      chain$ = chain$.pipe(
        switchMap(() => {
          return this.setUserRole(id, updateUserDto.role);
        }),
      );
    }

    return chain$.pipe(
      switchMap(() => {
        return this.findOne(id);
      }),
    );
  }

  /**
   * Deletes a user (if exists) and returns it
   *
   * @param {string} id
   * @return {*}  {Observable<void>}
   * @memberof UsersService
   */
  remove(id: string): Observable<void> {
    Logger.debug(`Removing user ${id}`);
    return this.http
      .delete(`${this.keycloakAdmin}/users/${id}`)
      .pipe(map((res) => res.data));
  }

  /**
   * Filter users by string contained in username, first or last name or email, returns the user or null
   *
   * @param email
   * @returns
   */
  searchUser(search: string): Observable<User> {
    return this.http
      .get(`${this.keycloakAdmin}/users?search=${search}`)
      .pipe(map((res) => res.data));
  }

  /**
   * Send an email to user with provided `user_id` with a link to
   * perform actions specified by `actions`
   *
   * @param user_id
   * @returns
   */
  public updateAccount(
    user_id: string,
    actions: accountActions[],
  ): Observable<any> {
    return this.http
      .put(
        `${this.keycloakAdmin}/users/${user_id}/execute-actions-email?lifespan=43200`,
        actions,
      )
      .pipe(map((res) => res.data));
  }

  // TODO: DOES NOT WORK, WRONG ENDPOINT
  public searchByRole(role: string): Observable<User[]> {
    return this.http
      .get(`${this.keycloakAdmin}/roles/{role-name}/users`)
      .pipe(map((res) => res.data));
  }

  public setUserRole(userId: string, role: string): Observable<void> {
    return this.getUserRoles(userId).pipe(
      switchMap((_roles: string[]) => this.deleteUserRoles(userId, _roles)),
      switchMap(() => this.findRoleByName(role)),
      switchMap((_role) => {
        return this.http
          .post<void>(
            `${this.keycloakAdmin}/users/${userId}/role-mappings/realm`,
            _role,
          )
          .pipe(map((res) => res.data));
      }),
    );
  }

  public deleteUserRoles(userId: string, _roles: any[]): Observable<void> {
    return this.http
      .delete(`${this.keycloakAdmin}/users/${userId}/role-mappings/realm`, {
        data: _roles,
      })
      .pipe(map((res) => res.data));
  }

  public findRoleByName(role: string): Observable<any> {
    return this.http
      .get(`${this.keycloakAdmin}/roles?search=${role}`)
      .pipe(map((res) => res.data));
  }

  public getUserRoles(userId: string): Observable<string[]> {
    return this.http
      .get(`${this.keycloakAdmin}/users/${userId}/role-mappings/realm`)
      .pipe(map((res) => res.data));
  }

  public getUserFromId(id: string): Observable<User> {
    return this.http
      .get(`${this.keycloakAdmin}/users/${id}`)
      .pipe(map((res) => res.data));
  }
}
