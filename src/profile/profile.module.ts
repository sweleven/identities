import { HttpModule, Module } from '@nestjs/common';
import { KeycloakModule } from 'src/keycloak/keycloak.module';
import { UsersModule } from 'src/users/users.module';
import { ProfileController } from './controllers/profile.controller';
import { ProfileService } from './services/profile.service';

@Module({
  imports: [HttpModule, KeycloakModule, UsersModule],
  controllers: [ProfileController],
  providers: [ProfileService],
})
export class ProfileModule {}
