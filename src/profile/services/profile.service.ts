import { HttpService, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { of } from 'rxjs';
import { catchError, filter, map, switchMap } from 'rxjs/operators';
import { KeycloakInterceptor } from 'src/keycloak/http.interceptor';
import { User } from 'src/users/entities/user.entity';
import { accountActions } from 'src/users/enums/accountActions.enum';
import { UsersService } from 'src/users/services/users.service';

@Injectable()
export class ProfileService {
  private readonly keycloakAdmin: string;

  constructor(
    private readonly http: HttpService,
    private readonly configService: ConfigService,
    private readonly keycloakInterceptor: KeycloakInterceptor,
    private readonly usersService: UsersService,
  ) {
    // Set interceptor to make authenticated calls
    this.http.axiosRef.interceptors.request.use((request) => {
      return this.keycloakInterceptor.authenticateRequest(request).toPromise();
    });

    // Get keycloak admin base path for all requests
    this.keycloakAdmin = this.configService.get('KEYCLOAK_ADMIN_PATH');
  }

  findUser(headers: Headers) {
    const userinfo = this._getUserinfo(headers);

    return this.usersService.searchUser(userinfo.username).pipe(
      map((users) => {
        if (users) {
          return users[0];
        } else {
          return null;
        }
      }),

      filter((_user) => _user != null),

      catchError((err, caught) => {
        // TODO: Manage returned status code to eventually infere error type
        Logger.log(err);
        return of(null);
      }),
    );
  }

  resetPassword(headers: Headers) {
    const userinfo = this._getUserinfo(headers);

    return this.usersService.searchUser(userinfo.username).pipe(
      map((users) => {
        if (users) {
          return users[0];
        } else {
          return null;
        }
      }),

      filter((_user) => _user != null),

      switchMap((user: User) => {
        return this.usersService
          .updateAccount(user.id, [accountActions.UPDATE_PASSWORD])
          .pipe(
            // Return user for coherence
            map(() => user),
          );
      }),
      catchError((err, caught) => {
        // TODO: Manage returned status code to eventually infere error type
        Logger.log(err);
        return of(null);
      }),
    );
  }

  private _getUserinfo(headers: Headers): any {
    const userinfo = headers['x-userinfo'];
    if (userinfo !== null && userinfo !== undefined) {
      return JSON.parse(Buffer.from(userinfo, 'base64').toString('binary'));
    } else {
      return 'internal';
    }
  }
}
