import { Controller, Get, Headers } from '@nestjs/common';
import { ProfileService } from '../services/profile.service';
@Controller('profile')
export class ProfileController {
  constructor(private readonly profileService: ProfileService) {}

  @Get()
  findAll(@Headers() headers: Headers) {
    return this.profileService.findUser(headers);
  }

  @Get('resetPassword')
  resetPassword(@Headers() headers: Headers) {
    return this.profileService.resetPassword(headers);
  }
}
