import { HttpService, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Observable, of } from 'rxjs';
import { tap, switchMap, map } from 'rxjs/operators';
import { JwtService } from '@nestjs/jwt';
import * as qs from 'qs';

@Injectable()
export class KeycloakInterceptor {
  private readonly keyloakBasePath: string;
  private readonly realm: string;
  private readonly keycloakAuthPath: string;
  private access_token: string;

  constructor(
    private readonly http: HttpService,
    private readonly configService: ConfigService,
    private readonly jwtService: JwtService,
  ) {
    this.keyloakBasePath = this.configService.get('KEYCLOAK_BASE_PATH');
    this.realm = this.configService.get('KEYCLOAK_DEFAULT_REALM');
    this.keycloakAuthPath = `${this.keyloakBasePath}/auth/realms/${this.realm}/protocol/openid-connect/token`;
  }

  /**
   * Hydrate request to keycloak with access token
   *
   * @private
   * @param {*} request
   * @return {*}  {Observable<any>}
   * @memberof UsersService
   */
  public authenticateRequest(request): Observable<any> {
    Logger.log(this.access_token);

    // Don't intercept requests to get authentication token
    if (request.url === this.keycloakAuthPath) {
      return of(request);
    }

    // Start observables chain
    let req = of('');

    // Extract current token expiration date
    const currentTokenExp = this.access_token
      ? this.jwtService.decode(this.access_token)['exp'] * 1000
      : 0;

    // Check if token is still valid
    if (currentTokenExp < new Date().getTime() + 1000) {
      // If it isn't valid anymore get another token
      req = req.pipe(
        tap(() => Logger.log('Token expired, authenticating')),
        switchMap(() => this._getAccessToken()),
        tap((access_token: string) => {
          this.access_token = access_token;
        }),
      );
    }

    return req.pipe(
      // Set bearer token on original request
      tap(() => {
        request.headers = {
          Authorization: `Bearer ${this.access_token}`,
          accept: 'application/json',
        };
      }),
      // Perform actual request
      switchMap(() => of(request)),
    );
  }

  /**
   * Authenticates against keycloak and returns access token
   *
   * @private
   * @return {*}
   * @memberof UsersService
   */
  private _getAccessToken(): Observable<string> {
    // Get config from env
    const client_id = this.configService.get('KEYCLOAK_CLIENT_ID');
    const client_secret = this.configService.get('KEYCLOAK_CLIENT_SECRET');
    const grant_type = this.configService.get('KEYCLOAK_GRANT_TYPE');

    // Urlencode params
    const body = qs.stringify({
      client_id,
      client_secret,
      grant_type,
    });

    // Get authentication token
    return this.http
      .post(this.keycloakAuthPath, body, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
      .pipe(
        // Return only access_token
        map((res) => res.data.access_token),
      );
  }
}
