import { ApiProperty } from '@nestjs/swagger';

export class User {
  @ApiProperty()
  id?: string;

  @ApiProperty()
  createdTimestamp?: Date;

  @ApiProperty()
  username: string;

  @ApiProperty()
  enabled?: string;

  @ApiProperty()
  totp?: string;
  emailVerified?: string;

  @ApiProperty()
  firstName?: string;

  @ApiProperty()
  lastName?: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  attributes: any;

  @ApiProperty()
  disabledCredentialTypes?: any;

  @ApiProperty()
  requiredActions?: any;

  @ApiProperty()
  notBefore?: any;

  @ApiProperty()
  access?: any;
}
