import { HttpModule, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { KeycloakInterceptor } from 'src/keycloak/http.interceptor';

@Module({
  imports: [HttpModule, JwtModule.register({})],
  providers: [KeycloakInterceptor],
  exports: [KeycloakInterceptor],
})
export class KeycloakModule {}
