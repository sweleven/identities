import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // Get configuration from Configuration Service
  const configService = app.get(ConfigService);

  // Setup OPENAPI
  const config = new DocumentBuilder()
    .setTitle('Identities')
    .setDescription('The BlockCOVID Identities microservice API description')
    .setVersion('1.0')
    .addServer('/api/identities')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('swagger', app, document);

  // Validate request's data with DTO
  // TODO: validate id with custom validator https://stackoverflow.com/questions/49709429/decorator-to-return-a-404-in-a-nest-controller
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
    }),
  );

  // Starting http
  const app_port = configService.get('PORT');
  await app.listen(app_port);
}
bootstrap();
