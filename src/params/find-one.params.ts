import { IsString } from 'class-validator';

export class FindOneParams {
  @IsString({
    message: 'Invalid id',
  })
  id: string;
}
