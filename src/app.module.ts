import * as Joi from '@hapi/joi';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { UsersModule } from './users/users.module';
import { ProfileModule } from './profile/profile.module';

@Module({
  imports: [
    // TODO: configure ConfigModule:
    //    - concept variables separation
    //    - improve validation
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema: Joi.object({
        PORT: Joi.number().required(),
        KEYCLOAK_BASE_PATH: Joi.string().required(),
        KEYCLOAK_DEFAULT_REALM: Joi.string().required(),
        KEYCLOAK_ADMIN_PATH: Joi.string().required(),
        KEYCLOAK_CLIENT_SECRET: Joi.string().required(),
        KEYCLOAK_CLIENT_ID: Joi.string().required(),
        KEYCLOAK_GRANT_TYPE: Joi.string().required(),
      }),
    }),
    UsersModule,
    ProfileModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
