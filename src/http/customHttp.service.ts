import { HttpService, Injectable } from '@nestjs/common';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class CustomHttpService {
  constructor(private readonly http: HttpService) {}

  protected post<T>(url: string, body: T): Observable<T> {
    return this.http.post<T>(url, body).pipe(map((res) => res.data));
  }

  protected get<T>(url: string, params = ''): Observable<T> {
    return this.http.get<T>(url + params).pipe(map((res) => res.data));
  }

  protected put<T>(url: string, params: string): Observable<T> {
    return this.http.put<T>(url, params).pipe(map((res) => res.data));
  }

  protected delete<T>(url: string, params: string): Observable<T> {
    return this.http.delete<T>(url + params).pipe(map((res) => res.data));
  }
}
