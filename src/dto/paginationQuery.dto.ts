import { IsOptional, IsPositive, IsString, IsIn } from 'class-validator';
import { Type } from 'class-transformer';

export class PaginationQueryDto {
  // Query params are string by default, casting to Number
  @Type(() => Number)
  @IsOptional()
  @IsPositive()
  limit: number;

  // Query params are string by default, casting to Number
  @Type(() => Number)
  @IsOptional()
  @IsPositive()
  offset: number;

  @IsOptional()
  @IsString({
    message: 'searchQuery must be a string',
  })
  searchQuery: string;

  @IsOptional()
  @IsString()
  @IsIn(['admin', 'employee', 'cleaner'])
  role: string;
}
